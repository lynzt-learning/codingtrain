CMD [ "python3", "-m", "http.server" ]

# build container
docker build -t py/ct .

# run script
docker run \
  --rm -ti \
  -p 8000:8000 \
  -v "$PWD":/usr/src/app \
  py/ct
