// learning core tf functionality

function setup() {
  noCanvas();

  const values = [];
  for (let i=0; i< 15; i++) {
    values[i] = random(0, 100);
  }
  const shape = [5, 3];

  // clean up tensors: a, a_t, c
  tf.tidy(() => {
    const a = tf.tensor(values, shape, 'int32');
    const a_t = a.transpose();

    const c = tf.matMul(a, a_t);
    c.print();
  })

}
