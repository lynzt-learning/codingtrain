// learning layers tf functionality
const model = tf.sequential();
const hidden = tf.layers.dense({
  units: 4,
  inputShape: [2],
  activation: 'sigmoid'
});

const output = tf.layers.dense({
  units: 1,
  activation: 'sigmoid'
});

model.add(hidden);
model.add(output);

// stochastic gradient descent
const sgdOpt = tf.train.sgd(0.1);
model.compile({
  optimizer: sgdOpt,
  loss: tf.losses.meanSquaredError
});

const X = tf.tensor2d([
  [0, 0],
  [.5, .5],
  [1, 1]
]);

const Y = tf.tensor2d([
  [0],
  [0.5],
  [1]
]);

train().then(() => {
  console.log (`training done...`);
  const guess = model.predict(X);
  guess.print();
});


async function train() {
  for (let i = 0; i < 1000; i++) {
    const response = await model.fit(X, Y, {
      shuffle: true,
      epochs: 10
    });
    if (i % 50 == 0) console.dir (response.history.loss[0]);
    // console.dir (response.history.loss[0]);
  }
}
// model.fit(X, Y)
//   .then((response) => {
//     console.dir (response.history.loss[0]);
//   });


// for (let i = 1; i < 5 ; ++i) {
//    const h = await model.fit(X, Y);
//    console.log("Loss after Epoch " + i + " : " + h.history.loss[0]);
// }




// const Y = model.predict(X);
// Y.print();
